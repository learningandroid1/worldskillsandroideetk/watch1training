package com.example.watch1

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast

class SignInActivity : Activity() {
    lateinit var email: EditText
    lateinit var pass: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        email = findViewById(R.id.email)
        pass = findViewById(R.id.pass)
    }

    fun sign(view: android.view.View) {
        if(email.text.isNotEmpty() && pass.text.isNotEmpty()){
             val perehod = Intent(this, ResultActivity::class.java)
            startActivity(perehod)
            finish()
        }
        else{
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка входа")
                .setMessage("У Вас есть пустые поля")
                .setPositiveButton("ok", null)
                .create()
                .show()

         //   Toast.makeText(this, "Ошибка входа", Toast.LENGTH_LONG).show()
        }
    }
}