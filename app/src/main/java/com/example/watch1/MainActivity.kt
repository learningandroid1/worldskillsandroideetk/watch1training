package com.example.watch1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import com.example.watch1.databinding.ActivityMainBinding

class MainActivity : Activity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

    fun login(view: android.view.View) {
        val perehod = Intent(this, SignInActivity::class.java)
        startActivity(perehod)
    }
}