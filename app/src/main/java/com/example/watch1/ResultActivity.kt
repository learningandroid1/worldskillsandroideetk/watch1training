package com.example.watch1

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class ResultActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
    }

    fun send(view: android.view.View) {
        Toast.makeText(this, "Успешно", Toast.LENGTH_SHORT).show()
    }
}